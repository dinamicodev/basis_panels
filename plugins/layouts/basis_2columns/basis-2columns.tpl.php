<?php
/**
 * @file
 * Template for Basis 3 Columns.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>

<div class="basis-twocol-layout <?php if (!empty($classes)) { print $classes; } ?><?php if (!empty($class)) { print $class; } ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

<div class="basis-twocol-layout__column1">
<?php print $content['column1']; ?>
</div>
<div class="basis-twocol-layout__column2">
<?php print $content['column2']; ?>
</div>
  
</div>
