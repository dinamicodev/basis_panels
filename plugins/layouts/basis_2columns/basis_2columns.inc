<?php
// Plugin definition
$plugin = array(
  'title' => t('Basis 2 columns'),
  'icon' => 'basis-2columns.png',
  'category' => t('Basis'),
  'theme' => 'basis_2columns',
  'regions' => array(
    'column1' => t('First Column'),
    'column2' => t('Second Column'),
  ),
);
