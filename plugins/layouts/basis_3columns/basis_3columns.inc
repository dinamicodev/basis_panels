<?php
// Plugin definition
$plugin = array(
  'title' => t('Basis 3 columns'),
  'icon' => 'basis-3columns.png',
  'category' => t('Basis'),
  'theme' => 'basis_3columns',
  'regions' => array(
    'column1' => t('First Column'),
    'column2' => t('Second Column'),
    'column3' => t('Third Column'),
  ),
);
